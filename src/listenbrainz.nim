## .. code-block:: nim
##  import listenbrainz
##  import listenbrainz/[core, stats, playlist, recordings, recommendation, social, status]
##
## `listenbrainz` provides multisync bindings for the `ListenBrainz.org<https://ListenBrainz.org>`_ web API.
##
## To find out more about each endpoint, as well as JSON documentation, refer to the `ListenBrainz API docs<https://listenbrainz.readthedocs.io/en/latest/dev/api/>`_.
##
## The following submodules are available for each set of endpoints:
##
## - `core<./listenbrainz/core.html>`_
## - `playlist<./listenbrainz/playlist.html>`_
## - `recordings<./listenbrainz/recordings.html>`_
## - `recommendation<./listenbrainz/recommendation.html>`_
## - `stats<./listenbrainz/stats.html>`_
## - `status<./listenbrainz/status.html>`_
## - `social<./listenbrainz/social.html>`_


import listenbrainz/utils/[client, api, jspfutils, tools]
import listenbrainz/[core, playlist, recordings, recommendation, stats, status, social]

export client
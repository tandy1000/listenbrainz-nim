import std/strutils
from std/httpcore import HttpMethod
import pkg/[union, uniony, asyncutils]
import utils/[client, api]
include utils/tools

type
  ListenType* = enum
    single = "single",
    playingNow = "playing_now",
    `import` = "import"

  ValidateToken* = ref object
    code*: int
    message*: string
    userName*: Option[string]
    valid*: bool

  SubmitListens* = ref object
    listenType*: ListenType
    payload*: seq[APIListen]

  RecentListens* = ref object
    payload*: RecentListensPayload

  UserListens* = ref object
    payload*: UserListensPayload

  UserListenCount = ref object
    payload*: UserListenCountPayload

  PlayingNow* = ref object
    payload*: PlayingNowPayload

  Listens* = ref object of RootObj
    count*: int
    listens*: seq[APIListen]

  RecentListensPayload* = ref object of Listens
    userList*: string

  UserListensPayload* = ref object of Listens
    latestListenTs*: int
    userId*: string

  UserListenCountPayload* = ref object of Listens
    playingNow*: bool
    userId*: string

  PlayingNowPayload* = ref object of Listens
    playingNow*: bool
    userId*: string

  SimilarUsers* = ref object
    payload*: seq[UserSimilarity]

  UsersSimilarTo* = ref object
    payload*: UserSimilarity

  UserSimilarity* = ref object
    similarity*: float
    userName*: string

func newSubmitListens*(
  listenType: ListenType,
  payload: seq[APIListen]): SubmitListens =
  ## Create new SubmissionPayload object
  result = SubmitListens(
    listenType: listenType,
    payload: payload
  )

proc submitListens*(
  lb: ListenBrainz,
  listens: SubmitListens): Future[string] {.fastsync.} =
  ## Submit listens to the server.
  ##
  ## `POST /1/submit-listens` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-submit-listens>`_)
  let payload = listens.toJson()
  if payload.len > MAX_LISTEN_SIZE:
    raise newException(IOError, "Submit listen payload over maximum size!")
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/submit-listens", HttpPost),
      body = payload
    )
    resp = await lb.request(req)
  return resp.body

proc validateToken*(
  lb: ListenBrainz,
  token: string): Future[ValidateToken] {.fastsync.} =
  ## Check whether a User Token is a valid entry in the database.
  ##
  ## `GET /1/validate-token` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-validate-token>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/validate-token", HttpGet, {"token": token}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(ValidateToken)

proc deleteListen*(
  lb: ListenBrainz,
  listenedAt: int,
  recordingMsid: string): Future[string] {.fastsync.} =
  ## Delete a particular listen from a user’s listen history.
  ##
  ## `POST /1/delete-listen` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-delete-listen>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/delete-listen", HttpPost),
      body = """{"listened_at": $#, "recording_msid": $#}""" % [$listenedAt, recordingMsid],
    )
    resp = await lb.request(req)
  return resp.body

proc getCollaborativePlaylists*(
  lb: ListenBrainz,
  username: string,
  count: int = DEFAULT_NUMBER_OF_PLAYLISTS_PER_CALL,
  offset: int = 0): Future[JSPFPayload] {.fastsync.}=
  ## Fetch playlist metadata in JSPF format without recordings for which a user is a collaborator.
  ##
  ## `GET /1/user/(playlist_username)/playlists/collaborator` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(playlist_user_name)-playlists-collaborator>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/playlists/collaborator" % username, HttpGet, {"count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(JSPFPayload)

proc getCreatedForPlaylists*(
  lb: ListenBrainz,
  username: string,
  count: int = DEFAULT_NUMBER_OF_PLAYLISTS_PER_CALL,
  offset: int = 0): Future[JSPFPayload] {.fastsync.}=
  ## Fetch playlist metadata in JSPF format without recordings that have been created for the user.
  ##
  ## `GET /1/user/(playlist_username)/playlists/createdfor` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(playlist_user_name)-playlists-createdfor>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/playlists/createdfor" % username, HttpGet, {"count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(JSPFPayload)

proc getSimilarUsers*(
  lb: ListenBrainz,
  username: string): Future[SimilarUsers] {.fastsync.} =
  ## Get the number of listens for a user `username`.
  ##
  ## `GET /1/user/(username)/similar-users` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(user_name)-similar-users>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/similar-users" % username, HttpGet, {"user_name": username}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(SimilarUsers)

proc getUserListenCount*(
  lb: ListenBrainz,
  username: string): Future[UserListenCount] {.fastsync.} =
  ## Get the number of listens for a user `username`.
  ##
  ## `GET /1/user/(username)/listen-count` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(user_name)-listen-count>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/listen-count" % username, HttpGet),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserListenCount)

proc getUserPlayingNow*(
  lb: ListenBrainz,
  username: string): Future[PlayingNow] {.fastsync.} =
  ## Get the listen being played right now for user `username`.
  ##
  ## `GET /1/user/(username)/playing-now` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(user_name)-playing-now>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/playing-now" % username, HttpGet),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(PlayingNow)

proc getUsersSimilarTo*(
  lb: ListenBrainz,
  username, otherUsername: string): Future[UsersSimilarTo] {.fastsync.} =
  ## Get the similarity of the user and the other user, based on their listening history.
  ##
  ## `GET /1/user/(username)/similar-to/(other_username)` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(user_name)-similar-to-(other_user_name)>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/similar-to/$#" % [username, otherUsername], HttpGet, {"user_name": username, "other_username": otherUsername}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UsersSimilarTo)

proc getUserPlaylists*(
  lb: ListenBrainz,
  username: string,
  count: int = DEFAULT_NUMBER_OF_PLAYLISTS_PER_CALL,
  offset: int = 0): Future[JSPFPayload] {.fastsync.} =
  ## Fetch playlist metadata in JSPF format without recordings for the given user.
  ##
  ## `GET /1/user/(playlist_username)/playlists` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(playlist_user_name)-playlists>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/playlists" % username, HttpGet, {"count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(JSPFPayload)

proc getUserListens*(
  lb: ListenBrainz,
  username: string,
  minTS, maxTS: int = 0,
  count: int = DEFAULT_ITEMS_PER_GET): Future[UserListens] {.fastsync.} =
  ## Get listens for user `username`.
  ##
  ## `GET /1/user/(username)/listens` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(user_name)-listens>`_)
  var params = @{"count": $min(count, MAX_ITEMS_PER_GET)}
  if minTS != 0:
    params.add ("min_ts", $minTS)
  if maxTS != 0:
    params.add ("max_ts", $maxTS)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/listens" % username, HttpGet, params),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserListens)

proc getLatestImport*(
  lb: ListenBrainz,
  username: string): Future[string] {.fastsync.} =
  ## Get the timestamp of the newest listen submitted by a user in previous imports to ListenBrainz.
  ##
  ## `GET /1/latest-import` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-latest-import>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/latest-import", HttpGet, {"user_name": username}),
    )
    resp = await lb.request(req)
  return resp.body

proc getLatestImport*(
  lb: ListenBrainz,
  ts: int): Future[string] {.fastsync.} =
  ## Update the timestamp of the newest listen submitted by a user in an import to ListenBrainz.
  ##
  ## `POST /1/latest-import` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-latest-import>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/latest-import", HttpPost),
      body = """{"ts": $#}""" % $ts,
    )
    resp = await lb.request(req)
  return resp.body

import std/strutils
from std/httpcore import HttpMethod
import pkg/[union, uniony, asyncutils]
import utils/[client, api]
include utils/tools

type
  UserTimelineEventType* = enum
    recordingRecommendation = "recording_recommendation",
    follow = "follow",
    listen = "listen",
    notification = "notification"

  EntityType* = enum
    recording = "recording"

  RecordingRecommendationMetadata* = ref object
    artistName*, trackName*, recordingMsid*, artistMsid*: string
    releaseName*, recordingMbid*: Option[string]

  NotificationMetadata* = ref object
    creator*, message*: string

  UserTimelineEvent* = ref object
    id*: Option[int]
    userId*: int
    eventType*: UserTimelineEventType
    userName*: string
    metadata*: union(RecordingRecommendationMetadata | NotificationMetadata)
    created*: Option[string]

  APINotificationEvent* = ref object
    message*: string

  APIFollowEvent* = ref object
    userName0*, userName1*, relationshipType*: string
    created*: int

  APITimelineEvent* = ref object
    eventType*: UserTimelineEventType
    userName*: string
    created*: int
    id*: Option[int]
    metadata*: union(APIListen | APIFollowEvent | APINotificationEvent)

  APITimeline* = ref object
    count*: int
    events*: seq[APITimelineEvent]
    userId*: string

  TimelineSubmissionPayload* = ref object
    metadata*: APITimelineRecommendation

  APITimelineRecommendation* = ref object
    artistName*, trackName*, recordingMsid*: string
    artistMsid*, releaseName*, recordingMbid*, blurbContent*: Option[string]
    users*: seq[string]

  TimelineEvents* = ref object
    payload*: APITimeline

  Followers* = ref object
    followers*: seq[string]
    user*: string

func newAPITimelineRecommendation*(
  artistName, trackName, recordingMsid: string,
  artistMsid, releaseName, recordingMbid, blurbContent: Option[string] = none(string),
  users: seq[string] = @[]): APITimelineRecommendation =
  result = APITimelineRecommendation(
    artistName: artistName,
    trackName: trackName,
    artistMsid: artistMsid,
    recordingMsid: recordingMsid,
    releaseName: releaseName,
    recordingMbid: recordingMbid,
    blurbContent: blurbContent,
    users: users
  )

proc recommendPersonal*(
  lb: ListenBrainz,
  username: string,
  recording: APITimelineRecommendation): Future[UserTimelineEvent] {.fastsync.} =
  ## Make the user recommend a recording to their followers.
  ##
  ## `POST /1/user/(username)/timeline-event/create/recording` (`API documentation<https://listenbrainz.readthedocs.io/en/latest/users/api/social.html#post--1-user-(user_name)-timeline-event-create-recommend-personal>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/timeline-event/create/recommend-personal" % username, HttpPost),
      body = TimelineSubmissionPayload(metadata: recording).toJson(),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserTimelineEvent)

proc postMessage*(
  lb: ListenBrainz,
  username, message: string): Future[string] {.fastsync.} =
  ## Post a message with a link on a user’s timeline.
  ##
  ## `POST /1/user/(username)/timeline-event/create/notification` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-user-(user_name)-timeline-event-create-notification>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/timeline-event/create/notification" % username, HttpPost),
      body = """{"metadata": {"message": "$#"}}""" % message,
    )
    resp = await lb.request(req)
  return resp.body

proc recommendRecording*(
  lb: ListenBrainz,
  username: string,
  recording: APITimelineRecommendation): Future[UserTimelineEvent] {.fastsync.} =
  ## Make the user recommend a recording to their followers.
  ##
  ## `POST /1/user/(username)/timeline-event/create/recording` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-user-(user_name)-timeline-event-create-recording>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/timeline-event/create/notification" % username, HttpPost),
      body = TimelineSubmissionPayload(metadata: recording).toJson(),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserTimelineEvent)

proc postReview*(
  lb: ListenBrainz,
  username, text, entityId, entityName, language: string,
  rating: int,
  entityType: EntityType,
  ): Future[string] {.fastsync.} =
  ## Creates a CritiqueBrainz review event for the user.
  ##
  ## `POST /1/user/(user_name)/timeline-event/create/review` (`API documentation<https://listenbrainz.readthedocs.io/en/latest/users/api/social.html#post--1-user-(user_name)-timeline-event-create-review>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/timeline-event/create/review" % username, HttpPost),
      body = """{"metadata":{"entity_name":"$#","entity_id":"$#","entity_type":"$#","language":"$#","rating":$#,"text":"$#"}}""" % [entityId, entityName, $entityType, language, $rating, text]
    )
    resp = await lb.request(req)
  return resp.body

proc deleteEvent*(
  lb: ListenBrainz,
  username: string,
  eventType: UserTimelineEventType,
  id: int): Future[string] {.fastsync.} =
  ## Delete those events from user’s feed that belong to them.
  ##
  ## `POST /1/user/(user_name)/feed/events/delete` (`API documentation<https://listenbrainz.readthedocs.io/en/latest/users/api/social.html#post--1-user-(user_name)-feed-events-delete>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/feed/events/delete" % username, HttpPost),
      body = """{"event_type": "$#", "id": $#}""" % [$eventType, $id],
    )
    resp = await lb.request(req)
  return resp.body

proc unhideEvent*(
  lb: ListenBrainz,
  username: string,
  eventType: UserTimelineEventType,
  id: int): Future[string] {.fastsync.} =
  ## Delete those events from user’s feed that belong to them.
  ##
  ## `POST /1/user/(user_name)/feed/events/unhide` (`API documentation<https://listenbrainz.readthedocs.io/en/latest/users/api/social.html#post--1-user-(user_name)-feed-events-unhide>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/feed/events/unhide" % username, HttpPost),
      body = """{"event_type": "$#", "id": $#}""" % [$eventType, $id],
    )
    resp = await lb.request(req)
  return resp.body

proc hideEvent*(
  lb: ListenBrainz,
  username: string,
  eventType: UserTimelineEventType,
  id: int): Future[string] {.fastsync.} =
  ## Delete those events from user’s feed that belong to them.
  ##
  ## `POST /1/user/(user_name)/feed/events/hide` (`API documentation<https://listenbrainz.readthedocs.io/en/latest/users/api/social.html#post--1-user-(user_name)-feed-events-hide>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/feed/events/hide" % username, HttpPost),
      body = """{"event_type": "$#", "id": $#}""" % [$eventType, $id],
    )
    resp = await lb.request(req)
  return resp.body

proc getEvents*(
  lb: ListenBrainz,
  username: string,
  maxTS, minTS: int = 0,
  count: int = DEFAULT_ITEMS_PER_GET): Future[TimelineEvents] {.fastsync.} =
  ## Get feed events for a user’s timeline.
  ##
  ## `GET /1/user/(username)/feed/events` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(user_name)-feed-events>`_)
  var params = @{"user_name": username, "count": $min(count, MAX_ITEMS_PER_GET)}
  if minTS != 0:
    params.add ("min_ts", $minTS)
  if maxTS != 0:
    params.add ("max_ts", $maxTS)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/feed/events" % username, HttpGet, params),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(TimelineEvents)

proc getFollowers*(
  lb: ListenBrainz,
  username: string): Future[Followers] {.fastsync.}=
  ## Fetch the list of followers of the user username.
  ##
  ## `GET /1/user/(username)/followers` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(user_name)-followers>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/followers" % username, HttpGet),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(Followers)

proc getFollowing*(
  lb: ListenBrainz,
  username: string): Future[Followers] {.fastsync.}=
  ## Fetch the list of users followed by the user username.
  ##
  ## `GET /1/user/(username)/following` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-user-(user_name)-following>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/following" % username, HttpGet),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(Followers)

proc unfollow*(
  lb: ListenBrainz,
  username: string): Future[string] {.fastsync.}=
  ## Unfollow the user username.
  ##
  ## `GET /1/user/(username)/unfollow` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-user-(user_name)-unfollow>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/unfollow" % username, HttpPost),
    )
    resp = await lb.request(req)
  return resp.body

proc follow*(
  lb: ListenBrainz,
  username: string): Future[string] {.fastsync.}=
  ## Follow the user username.
  ##
  ## `GET /1/user/(username)/follow` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-user-(user_name)-follow>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/user/$#/follow" % username, HttpPost),
    )
    resp = await lb.request(req)
  return resp.body

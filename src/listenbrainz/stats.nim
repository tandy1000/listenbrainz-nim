import std/strutils
from std/httpcore import HttpMethod
import pkg/asyncutils
import utils/[client, api]
include utils/tools

type
  StatsRangeType* = enum
    week = "week",
    month = "month",
    year = "year",
    allTime = "all_time"

  UserStatsType* = enum
    artists = "artists",
    releases = "releases",
    recordings = "recordings",
    dailyActivity = "daily_activity",
    listeningActivity = "listening_activity",
    artistMap = "artist_map"

  SitewideTopArtists* = ref object
    payload*: SitewideTopArtistsPayload

  UserListeningActivity* = ref object
    payload*: UserListeningActivityPayload

  UserDailyActivity* = ref object
    payload*: UserDailyActivityPayload

  UserTopRecordings* = ref object
    payload*: UserTopRecordingsPayload

  UserTopArtists* = ref object
    payload*: UserTopArtistsPayload

  UserTopReleases* = ref object
    payload*: UserTopReleasesPayload

  UserArtistMap* = ref object
    payload*: UserArtistMapPayload

  Payload* = ref object of RootObj
    toTs*, fromTs*: int
    `range`: StatsRangeType
    userId*: string

  SitewideTopArtistsPayload* = ref object of Payload
    artists*: seq[ArtistStats]
    count*, offset*, lastUpdated*: int
    totalArtistCount*: int

  UserListeningActivityPayload* = ref object of Payload
    lastUpdated*: int
    listeningActivity*: seq[ListenStats]

  UserDailyActivityPayload* = ref object of Payload
    lastUpdated*: int
    dailyActivity*: DailyActivityStats

  UserTopRecordingsPayload* = ref object of Payload
    count*, offset*, totalRecordingCount*, lastUpdated*: int
    recordings*: seq[UserRecordingStats]

  UserTopArtistsPayload* = ref object of Payload
    count*, offset*, totalArtistCount*, lastUpdated*: int
    recordings*: seq[ArtistStats]

  UserTopReleasesPayload* = ref object of Payload
    count*, offset*, totalReleaseCount*, lastUpdated*: int
    releases*: seq[ReleaseStats]

  UserArtistMapPayload* = ref object of Payload
    lastUpdated*: int
    artistMap*: seq[CountryStats]

  DailyActivityStats* = ref object
    `Sunday`*, `Monday`*, `Tuesday`*, `Wednesday`*, `Thursday`*, `Friday`*, `Saturday`*: seq[DayActivityStats]

  DayActivityStats* = ref object
    hour*, listenCount*: int

  ArtistStats* = ref object
    artistMbids*: Option[seq[string]]
    artistMsid*: Option[string]
    artistName*: string
    listenCount*: int

  ListenStats* = ref object
    toTs*, fromTs*, listenCount*: int
    timeRange*: string

  UserRecordingStats* = ref object
    artistMbids*: seq[string]
    artistMsid*, recordingMbid*, recordingMsid*, releaseMbid*, releaseMsid*: Option[string]
    artistName*, releaseName*, trackName*: string
    listenCount*: int

  ReleaseStats* = ref object
    artistMbids*: Option[seq[string]]
    artistMsid*, releaseMbid*, releaseMsid*: Option[string]
    artistName*, releaseName*: string
    listenCount*: int

  CountryStats* = ref object
    artistCount*, listenCount*: int
    country*: string

proc getSitewideTopArtists*(
  lb: ListenBrainz,
  dateRange: StatsRangeType,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[SitewideTopArtists] {.fastsync.} =
  ## Get top artists sitewide.
  ##
  ## `GET /1/stats/sitewide/artists` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-stats-sitewide-artists>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/stats/sitewide/artists", HttpGet, {"count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset, "range": $dateRange}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(SitewideTopArtists)

proc getUserListeningActivity*(
  lb: ListenBrainz,
  username: string,
  dateRange: StatsRangeType): Future[UserListeningActivity] {.fastsync.} =
  ## Get the listening activity for user `username`.
  ##
  ## `GET /1/stats/user/(username)/listening-activity` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-stats-user-(user_name)-listening-activity>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/stats/user/$#/listening-activity" % username, HttpGet, {"user_name": username, "range": $dateRange}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserListeningActivity)

proc getUserDailyActivity*(
  lb: ListenBrainz,
  username: string,
  dateRange: StatsRangeType): Future[UserDailyActivity] {.fastsync.} =
  ## Get the daily activity for user `username`.
  ##
  ## `GET /1/stats/user/(username)/daily-activity` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-stats-user-(user_name)-daily-activity>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/stats/user/$#/daily-activity" % username, HttpGet, {"user_name": username, "range": $dateRange}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserDailyActivity)

proc getUserTopRecordings*(
  lb: ListenBrainz,
  username: string,
  dateRange: StatsRangeType,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[UserTopRecordings] {.fastsync.} =
  ## Get top recordings for user `username`.
  ##
  ## `GET /1/stats/user/(username)/recordings`  (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-stats-user-(user_name)-recordings>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/stats/user/$#/recordings" % username, HttpGet, {"user_name": username, "range": $dateRange, "count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserTopRecordings)

proc getUserArtistMap*(
  lb: ListenBrainz,
  username: string,
  dateRange: StatsRangeType,
  forceRecalculate: bool = false): Future[UserArtistMap] {.fastsync.} =
  ## Get artist map for user `username`.
  ##
  ## `GET /1/stats/user/(username)/artist-map`  (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-stats-user-(user_name)-artist-map>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/stats/user/$#/artist-map" % username, HttpGet, {"user_name": username, "range": $dateRange, "force_recalculate": $forceRecalculate}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserArtistMap)

proc getUserTopReleases*(
  lb: ListenBrainz,
  username: string,
  dateRange: StatsRangeType,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[UserTopReleases] {.fastsync.} =
  ## Get top releases for user `username`.
  ##
  ## `GET /1/stats/user/(username)/releases` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-stats-user-(user_name)-releases>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/stats/user/$#/releases" % username, HttpGet, {"user_name": username, "range": $dateRange, "count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserTopReleases)

proc getUserTopArtists*(
  lb: ListenBrainz,
  username: string,
  dateRange: StatsRangeType,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[UserTopArtists] {.fastsync.} =
  ## Get top artists for user `username`.
  ##
  ## `GET /1/stats/user/(username)/artists` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-stats-user-(user_name)-artists>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/stats/user/$#/artists" % username, HttpGet, {"user_name": username, "count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset, "range": $dateRange}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(UserTopArtists)
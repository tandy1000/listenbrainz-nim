# C backend support
import std/[os, httpclient, strutils]
import pkg/asyncutils
import pure
import ".."/api

type
  SyncListenBrainz* = ListenBrainz[HttpClient]
  AsyncListenBrainz* = ListenBrainz[AsyncHttpClient]

ListenBrainz.setSync(SyncListenBrainz)
ListenBrainz.setAsync(AsyncListenBrainz)

proc setDefaultHeaders(lb: ListenBrainz) =
  ## Sets default headers
  if lb.token != "":
    lb.http.headers["Authorization"] = "Token " & lb.token

proc setContentType*(lb: ListenBrainz, contentType: string) =
  ## Sets content type header
  lb.http.headers["Content-Type"] = contentType

proc newSyncListenBrainz*(
  token: string = "",
  baseUrl: string = apiRoot): SyncListenBrainz =
  ## Create a new `SyncListenBrainz` object
  new(result)
  result.http = newHttpClient()
  result.token = token
  result.baseUrl = baseUrl
  result.setDefaultHeaders()

proc newAsyncListenBrainz*(
  token: string = "",
  baseUrl: string = apiRoot): AsyncListenBrainz =
  ## Create a new `AsyncListenBrainz` object
  new(result)
  result.http = newAsyncHttpClient()
  result.token = token
  result.baseUrl = baseUrl
  result.setDefaultHeaders()

proc handleRateLimit(
  lb: ListenBrainz,
  req: PureRequest,
  headers: HttpHeaders): Future[PureResponse] {.fastsync.} =
  let resetMs = parseInt(headers["x-ratelimit-reset"])*1000

  while true:
    when lb is AsyncListenBrainz:
      await sleepAsync(resetMs)
    else:
      os.sleep(resetMs)
    let
      resp = await lb.http.request(
        url = $req.endpoint,
        httpMethod = req.endpoint.httpMethod,
        body = req.body
      )
      code = resp.code()
      payload = await resp.body()

    if not code.is2xx():
      responseCheck(code, payload)
    else:
      return PureResponse(
        code: code,
        body: payload,
        headers: resp.headers
      )

proc request*(
  lb: ListenBrainz,
  req: PureRequest): Future[PureResponse] {.fastsync.} =
  let
    resp = await lb.http.request(
      url = $req.endpoint,
      httpMethod = req.endpoint.httpMethod,
      headers = lb.http.headers,
      body = req.body
    )
    code = resp.code()
    payload = await resp.body()

  if not code.is2xx():
    if code == Http429:
      return await lb.handleRateLimit(req, resp.headers)
    responseCheck(code, payload)

  # Parse their response and give it back as a PureResponse
  return PureResponse(
    code: code,
    body: payload,
    headers: resp.headers,
  )

export pure

## Frontend JavaScript support
import
  std/[dom, strutils, jsheaders, httpcore, tables, sequtils],
  pkg/nodejs/jshttpclient,
  pkg/asyncutils,
  pure,
  ../api

type
  SyncListenBrainz* = ListenBrainz[JsHttpClient]
  AsyncListenBrainz* = ListenBrainz[JsAsyncHttpClient]

ListenBrainz.setSync(SyncListenBrainz)
ListenBrainz.setAsync(AsyncListenBrainz)

proc setDefaultHeaders(lb: ListenBrainz) =
  ## Sets default headers
  if lb.token != "":
    lb.http.headers["Authorization".cstring] = cstring("Token " & lb.token)

proc setContentType*(lb: ListenBrainz, contentType: string) =
  ## Sets content type header
  lb.http.headers["Content-Type".cstring] = cstring(contentType)

proc newListenBrainz*(
  token: string = "",
  baseUrl: string = apiRoot): SyncListenBrainz =
  ## Create a new `AsyncListenBrainz` object
  new(result)
  result.http = newJsHttpClient()
  result.token = token
  result.baseUrl = baseUrl
  result.setDefaultHeaders()

proc newAsyncListenBrainz*(
  token: string = "",
  baseUrl: string = apiRoot): AsyncListenBrainz =
  ## Create a new `AsyncListenBrainz` object
  new(result)
  result.http = newJsAsyncHttpClient()
  result.token = token
  result.baseUrl = baseUrl
  result.setDefaultHeaders()

proc setTimeoutSync(ms: int) = {.emit: "setTimeout(function() { }, `ms`);".}

proc setTimeoutAsync(ms: int): Future[void] =
  let promise = newPromise() do (res: proc(): void):
    discard setTimeout(res, ms)
  return promise

proc parseHeaders(headers: Headers): HttpHeaders =
  var httpHeaders = newHttpHeaders()
  let ckeys = headers.keys()
  for ckey in ckeys:
    let
      cval = headers[ckey]
      key = $ckey
      val = $cval
    httpHeaders[key] = val
  return httpHeaders

proc newRequest(req: PureRequest): JsRequest =
  return newJsRequest(
    url = cstring($req.endpoint),
    `method` = req.endpoint.httpMethod,
    body = cstring(req.body)
  )

proc handleRateLimit(
  lb: ListenBrainz,
  req: PureRequest,
  respHeaders: Headers): Future[PureResponse] {.fastsync.} =
  let resetMs = 1000 * parseInt($respHeaders["x-ratelimit-reset-in"])
  while true:
    when lb is AsyncListenBrainz:
      await setTimeoutAsync(resetMs)
    else:
      setTimeoutSync(resetMs)

    let
      newReq = newRequest(req)
      resp = await lb.http.request(newReq)
      payload = resp.responseText
      code = resp.status

    if not code.is2xx():
      responseCheck(code, $payload)
    else:
      let headers = parseHeaders(resp.headers)
      return PureResponse(
        code: code,
        body: $payload,
        headers: headers
      )

proc request*(
  lb: ListenBrainz,
  req: PureRequest): Future[PureResponse] {.fastsync.} =
  let
    newReq = newRequest(req)
    resp = await lb.http.request(newReq)
    payload = resp.responseText
    code = resp.status

  if not code.is2xx():
    if code == Http429:
      return await lb.handleRateLimit(req, resp.headers)
    responseCheck(code, $payload)

  # Parse their response and give it back as a PureResponse
  let headers = parseHeaders(resp.headers)
  return PureResponse(
    code: code,
    body: $payload,
    headers: headers
  )

export pure
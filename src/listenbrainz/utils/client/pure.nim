import std/[uri, httpcore, json]

type
  ListenBrainz*[T] = ref object
    http*: T
    token*, baseUrl*: string

  Endpoint* {.pure.} = ref object
    target*: Uri
    httpMethod*: HttpMethod

  PureRequest* = ref object
    endpoint*: Endpoint
    body*: string

  PureResponse* = ref object
    code*: HttpCode
    body*: string
    headers*: HttpHeaders

  HttpRequestError* = object of IOError

func newPureRequest*(endpoint: Endpoint, body: string = ""): PureRequest =
  result = PureRequest(endpoint: endpoint, body: body)

func `$`*(e: Endpoint): string =
  return $e.target

func build*(
  apiRoot, path: string,
  httpMethod: HttpMethod,
  params: varargs[(string, string)] = []): Endpoint =
  ## Builds an endpoint given the API root, path, `httpMethod`, and any query parameters
  var uri = parseUri(apiRoot & path)
  uri.query = encodeQuery(params)
  return Endpoint(target: uri, httpMethod: httpMethod)

proc responseCheck*(code: HttpCode, body: string) =
  ## Raises exceptions and relevant error messages if status is not 200.
  var error: string
  try:
    error = getStr parseJson(body)["error"]
  except JsonParsingError:
    error = "Unable to parse the error:\n" & body
  case code:
  of Http400:
    raise newException(HttpRequestError, "ERROR 400 Bad Request\n" & error)
  of Http401:
    raise newException(HttpRequestError, "ERROR 401 Unauthorized\n" & error)
  of Http403:
    raise newException(HttpRequestError, "ERROR 403 Forbidden\n" & error)
  of Http404:
    raise newException(HttpRequestError, "ERROR 404 Not Found\n" & error)
  of Http405:
    raise newException(HttpRequestError, "ERROR 405 Method Not Allowed\n" & error)
  of Http429:
    raise newException(HttpRequestError, "ERROR 429 Too many Requests\n" & error)
  of Http500:
    raise newException(HttpRequestError, "ERROR 500 Data requested not available\n" & error)
  of Http503:
    raise newException(HttpRequestError, "ERROR 503 Cannot submit listens to queue, please try again later.\n" & error)
  else:
    raise newException(HttpRequestError, "ERROR Received an unexpected status response\n" & error)

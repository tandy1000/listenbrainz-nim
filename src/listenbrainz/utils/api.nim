import
  std/options,
  pkg/union

const
  apiRoot* = "https://api.listenbrainz.org"
  webRoot* = "https://listenbrainz.org"
  DEFAULT_ITEMS_PER_GET* = 25
  DEFAULT_NUMBER_OF_PLAYLISTS_PER_CALL* = 25
  MAX_RECORDINGS_PER_ADD* = 100
  MAX_ITEMS_PER_GET* = 100
  MAX_TAGS_PER_LISTEN* = 50
  MAX_TAG_SIZE* = 64
  MAX_LISTEN_SIZE* = 10240

type
  APIListen* = object
    listenedAt*, insertedAt*: Option[int]
    userName*, listenedAtIso*, recordingMsid*: Option[string]
    playingNow*: Option[bool]
    trackMetadata*: TrackMetadata

  TrackMetadata* = object
    trackName*, artistName*: string
    releaseName*: Option[string]
    additionalInfo*: Option[AdditionalInfo]

  AdditionalInfo* = object
    artistMbids*: Option[seq[string]]
    releaseGroupMbid*: Option[string]
    releaseMbid*: Option[string]
    recordingMbid*: Option[string]
    trackMbid*: Option[string]
    workMbids*: Option[seq[string]]
    tracknumber*: union(Option[string] | Option[int])
    isrc*: Option[string]
    spotifyId*: Option[string]
    tags*: Option[seq[string]]
    mediaPlayer*: Option[string]
    mediaPlayerVersion*: Option[string]
    submissionClient*: Option[string]
    submissionClientVersion*: Option[string]
    musicService*: Option[string]
    musicServiceName*: Option[string]
    originUrl*: Option[string]

func newAPIListen*(
  listenedAt, insertedAt: Option[int] = none(int),
  userName, listenedAtIso, recordingMsid: Option[string] = none(string),
  playingNow: Option[bool] = none(bool),
  trackMetadata: TrackMetadata): APIListen =
  ## Create new Listen object
  result = APIListen(
    listenedAt: listenedAt,
    insertedAt: insertedAt,
    userName: userName,
    listenedAtIso: listenedAtIso,
    recordingMsid: recordingMsid,
    playingNow: playingNow,
    trackMetadata: trackMetadata
  )

func `==`*(a, b: APIListen): bool =
  return a.listenedAt == b.listenedAt and
    a.insertedAt == b.insertedAt and
    a.userName == b.userName and
    a.listenedAtIso == b.listenedAtIso and
    a.recordingMsid == b.recordingMsid and
    a.playingNow == b.playingNow and
    a.trackMetadata == b.trackMetadata

func newAdditionalInfo*(
  artistMbids = none(seq[string]),
  releaseGroupMbid = none(string),
  releaseMbid = none(string),
  recordingMbid = none(string),
  trackMbid = none(string),
  workMbids = none(seq[string]),
  tracknumber = none(string) as union(Option[string] | Option[int]),
  isrc = none(string),
  spotifyId = none(string),
  tags = none(seq[string]),
  mediaPlayer = none(string),
  mediaPlayerVersion = none(string),
  submissionClient = none(string),
  submissionClientVersion = none(string),
  musicService = none(string),
  musicServiceName = none(string),
  originUrl = none(string)
 ): AdditionalInfo =
  ## Create new Track object
  result = AdditionalInfo(
    artistMbids: artistMbids,
    releaseGroupMbid: releaseGroupMbid,
    releaseMbid: releaseMbid,
    recordingMbid: recordingMbid,
    trackMbid: trackMbid,
    workMbids: workMbids,
    tracknumber: tracknumber,
    isrc: isrc,
    spotifyId: spotifyId,
    tags: tags,
    mediaPlayer: mediaPlayer,
    mediaPlayerVersion: mediaPlayerVersion,
    submissionClient: submissionClient,
    submissionClientVersion: submissionClientVersion,
    musicService: musicService,
    musicServiceName: musicServiceName,
    originUrl: originUrl
  )

func `==`*(a, b: AdditionalInfo): bool =
  return a.artistMbids == b.artistMbids and
    a.releaseGroupMbid == b.releaseGroupMbid and
    a.releaseMbid == b.releaseMbid and
    a.recordingMbid == b.recordingMbid and
    a.trackMbid == b.trackMbid and
    a.workMbids == b.workMbids and
    a.tracknumber == b.tracknumber and
    a.isrc == b.isrc and
    a.spotifyId == b.spotifyId and
    a.tags == b.tags and
    a.mediaPlayer == b.mediaPlayer and
    a.mediaPlayerVersion == b.mediaPlayerVersion and
    a.submissionClient == b.submissionClient and
    a.submissionClientVersion == b.submissionClientVersion and
    a.musicService == b.musicService and
    a.musicServiceName == b.musicServiceName and
    a.originUrl == b.originUrl

func newTrackMetadata*(
  trackName, artistName: string,
  releaseName: Option[string] = none(string),
  additionalInfo: Option[AdditionalInfo] = some(newAdditionalInfo())): TrackMetadata =
  ## Create new TrackMetadata object
  result = TrackMetadata(
    trackName: trackName,
    artistName: artistName,
    releaseName: releaseName,
    additionalInfo: additionalInfo
  )

func `==`*(a, b: TrackMetadata): bool =
  return a.trackName == b.trackName and
    a.artistName == b.artistName and
    a.releaseName == b.releaseName and
    a.additionalInfo == b.additionalInfo

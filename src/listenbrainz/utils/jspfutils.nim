import
  std/options,
  pkg/jspf

type
  JSPFPayload* = ref object
    playlist*: Playlist

  Playlist* = ref object of JSPFPlaylist
    track*: seq[Track]
    extension*: PlaylistExtension

  Track* = ref object of JSPFTrack
    extension*: TrackExtension

  PlaylistExtension* = ref object
    playlistExtensionUrl*: MusicBrainzPlaylistExtension

  TrackExtension* = ref object
    trackExtensionUrl*: MusicBrainzTrackExtension

  MusicBrainzPlaylistExtension* = ref object
    collaborators*: Option[seq[string]]
    creator*, createdFor*, copiedFrom*, lastModifiedAt*: Option[string]
    copiedFromDeleted*: Option[bool]
    public*: bool

  MusicBrainzTrackExtension* = ref object
    releaseIdentifier*, addedBy*, addedAt*: Option[string]
    artistMbids*: Option[seq[string]]

func newJSPFPayload*(playlist: Playlist): JSPFPayload = JSPFPayload(playlist: playlist)

func newMusicBrainzPlaylistExtension*(
  public: bool = true,
  collaborators: Option[seq[string]] = none(seq[string]),
  creator, createdFor, copiedFrom, lastModifiedAt: Option[string] = none(string),
  copiedFromDeleted: Option[bool] = none(bool)): MusicBrainzPlaylistExtension =
  result = MusicBrainzPlaylistExtension(
    creator: creator,
    createdFor: createdFor,
    collaborators: collaborators,
    copiedFrom: copiedFrom,
    lastModifiedAt: lastModifiedAt,
    copiedFromDeleted: copiedFromDeleted,
    public: public
  )

func newPlaylistExtension*(playlistExtensionUrl: MusicBrainzPlaylistExtension = newMusicBrainzPlaylistExtension()): PlaylistExtension = PlaylistExtension(playlistExtensionUrl: playlistExtensionUrl)

func newPlaylist*(
  title: string,
  track: seq[Track] = @[],
  creator, annotation, info, image, date, license, identifier, location: Option[string] = none(string),
  meta, attribution, link: Option[seq[JSPFAttributes]] = none(seq[JSPFAttributes]),
  extension: PlaylistExtension = newPlaylistExtension()): Playlist =
  result = Playlist(
    track: track,
    title: some title,
    creator: creator,
    annotation: annotation,
    info: info,
    image: image,
    date: date,
    license: license,
    identifier: identifier,
    location: location,
    meta: meta,
    attribution: attribution,
    link: link,
    extension: extension
  )

func newMusicBrainzTrackExtension*(
  artistMbids: Option[seq[string]] = none(seq[string]),
  releaseIdentifier, addedBy, addedAt: Option[string] = none(string)): MusicBrainzTrackExtension =
  result = MusicBrainzTrackExtension(
    releaseIdentifier: releaseIdentifier,
    addedBy: addedBy,
    addedAt: addedAt,
    artistMbids: artistMbids
  )

func newTrackExtension*(trackExtensionUrl: MusicBrainzTrackExtension = newMusicBrainzTrackExtension()): TrackExtension = TrackExtension(trackExtensionUrl: trackExtensionUrl)

func newTrack*(
  identifier: string,
  title, creator, annotation, info, image, album, location: Option[string] = none(string),
  trackNum, duration: Option[int] = none(int),
  meta, link: Option[JSPFAttributes] = none(JSPFAttributes),
  extension: TrackExtension = newTrackExtension()): Track =
  result = Track(
    title: title,
    creator: creator,
    annotation: annotation,
    info: info,
    image: image,
    album: album,
    trackNum: trackNum,
    duration: duration,
    identifier: some("https://musicbrainz.org/recording/" & identifier),
    location: location,
    meta: meta,
    link: link,
    extension: extension
  )

import std/strutils
from std/httpcore import HttpMethod
import pkg/asyncutils
import utils/[client, api]
include utils/tools

type
  RecommendationFeedbackType* = enum
    like = "like",
    love = "love",
    dislike = "dislike",
    hate = "hate",
    badRecommendation = "bad_recommendation"

  RecommendationArtistType* = enum
    top = "top",
    similar = "similar"

  RecordingRecommendation* = ref object
    payload*: RecordingRecommendationPayload

  RecordingRecommendationPayload* = ref object
    lastUpdated*, score*, count*, totalMbidCount*, offset*: int
    artistType*: RecommendationArtistType
    entity*, userName*: string
    mbids*: seq[RecordingRecommendationFeedback]

  RecordingRecommendationFeedback* = ref object
    recordingMbid*: string
    score*: float

  RecommendationFeedbackPayload* = ref object
    count*, offset*, totalCount*: int
    feedback*: seq[RecommendationFeedback]

  RecommendationFeedback* = ref object
    created*: int
    rating*: RecommendationFeedbackType
    recordingMbid*: string

proc getUserRecommendations*(
  lb: ListenBrainz,
  username: string,
  artistType: RecommendationArtistType,
  count: int = DEFAULT_NUMBER_OF_PLAYLISTS_PER_CALL,
  offset: int = 0): Future[RecordingRecommendation] {.fastsync.} =
  ## Get recommendations sorted on rating and ratings for user username.
  ##
  ## `GET /1/cf/recommendation/user/(username)/recording` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-cf-recommendation-user-(user_name)-recording>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/cf/recommendation/user/$#/recording" % username, HttpGet, {"artist_type": $artistType, "count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(RecordingRecommendation)

proc submitRecommendationFeedback*(
  lb: ListenBrainz,
  recordingMbid: string,
  rating: RecommendationFeedbackType): Future[string] {.fastsync.} =
  ## Submit recommendation feedback.
  ##
  ## `POST /1/recommendation/feedback/submit` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-recommendation-feedback-submit>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/recommendation/feedback/submit", HttpPost),
      body = """{"recording_mbid": "$#", "rating": "$#"}""" % [recordingMbid, $rating],
    )
    resp = await lb.request(req)
  return resp.body

proc deleteRecommendationFeedback*(
  lb: ListenBrainz,
  recordingMbid: string): Future[string] {.fastsync.} =
  ## Delete feedback for a user.
  ##
  ## `POST /1/recommendation/feedback/delete` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-recommendation-feedback-delete>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/recommendation/feedback/delete", HttpPost),
      body = """{"recording_mbid": "$#"}""" % recordingMbid,
    )
    resp = await lb.request(req)
  return resp.body

proc getFeedbackForRecordings*(
  lb: ListenBrainz,
  username: string,
  recordings: seq[string]): Future[RecommendationFeedbackPayload] {.fastsync.} =
  ## Get feedback given by user username for the list of recordings supplied.
  ##
  ## `GET /1/recommendation/feedback/user/(username)/recordings` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-recommendation-feedback-user-(user_name)-recordings>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/recommendation/feedback/user/$#/recordings" % username, HttpGet, {"mbids": join(recordings, ",")}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(RecommendationFeedbackPayload)

proc getFeedback*(
  lb: ListenBrainz,
  username: string,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[RecommendationFeedbackPayload] {.fastsync.} =
  ## Get feedback given by user username.
  ##
  ## `GET /1/recommendation/feedback/user/(username)` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-recommendation-feedback-user-(user_name)>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/recommendation/feedback/user/$#" % username, HttpGet, {"count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(RecommendationFeedbackPayload)

proc getFeedback*(
  lb: ListenBrainz,
  username: string,
  rating: RecommendationFeedbackType,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[RecommendationFeedbackPayload] {.fastsync.} =
  ## Get feedback given by user username.
  ##
  ## `GET /1/recommendation/feedback/user/(username)` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-recommendation-feedback-user-(user_name)>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/recommendation/feedback/user/$#" % username, HttpGet, {"rating": $rating, "count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(RecommendationFeedbackPayload)

import std/strutils
from std/httpcore import HttpMethod
import pkg/asyncutils
import utils/[client, api]
include utils/tools

type
  Feedback* = ref object
    count*, offset*, totalCount*: Option[int]
    feedback*: seq[RecordingFeedback]

  RecordingFeedback* = ref object
    recordingMsid*: string
    score*: int
    created*: Option[int]
    userId*: Option[string]
    trackMetadata*: Option[TrackMetadata]

  PinnedRecordings* = ref object
    count*, offset*: int
    totalCount*: Option[int]
    pinnedRecordings*: seq[PinnedRecording]
    userName*: string

  PinnedRecording* = ref object
    pinnedRecording*: PinnedRecordingPayload

  PinnedRecordingPayload* = ref object
    blurbContent*: string
    recordingMsid*, recordingMbid*, userName*: Option[string]
    created*, pinnedUntil*, rowId*: Option[int]
    trackMetadata: Option[TrackMetadata]

func newRecordingFeedback*(
  score: int,
  recordingMsid: string): RecordingFeedback =
  result = RecordingFeedback(
    recordingMsid: recordingMsid,
    score: score
  )

func scoreValidate*(
  score: int) =
  ## Raises exceptions and relevant error message if score is invalid.
  if score < -1 or score > 1:
    raise newException(ValueError, "ERROR score must be -1 (hate), 0 (none), or 1 (love)")

func newPinnedRecordingPayload*(
  recordingMsid, blurbContent: string,
  recordingMbid: Option[string] = none(string),
  pinnedUntil: int): PinnedRecordingPayload =
  result = PinnedRecordingPayload(
    recordingMsid: some(recordingMsid),
    blurbContent: blurbContent,
    recordingMbid: recordingMbid,
    pinnedUntil: some(pinnedUntil)
  )

proc submitRecordingFeedback*(
  lb: ListenBrainz,
  recordingFeedback: RecordingFeedback): Future[string] {.fastsync.} =
  ## Submit recording feedback (love/hate) to the server.
  ##
  ## `POST /1/feedback/recording-feedback` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-feedback-recording-feedback>`_)
  scoreValidate(recordingFeedback.score)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/feedback/recording-feedback", HttpPost),
      body = recordingFeedback.toJson(),
    )
    resp = await lb.request(req)
  return resp.body

proc getRecordingFeedback*(
  lb: ListenBrainz,
  recordingMsid: string,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[Feedback] {.fastsync.} =
  ## Get feedback for recording with given recording_msid.
  ##
  ## `GET /1/feedback/recording/(recording_msid)/get-feedback` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-feedback-recording-(recording_msid)-get-feedback>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/feedback/recording/$#/get-feedback" % recordingMsid, HttpGet, {"count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(Feedback)

proc getRecordingFeedback*(
  lb: ListenBrainz,
  recordingMsid: string,
  score: int,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[Feedback] {.fastsync.} =
  ## Get feedback for recording with given recording_msid.
  ##
  ## `GET /1/feedback/recording/(recording_msid)/get-feedback` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-feedback-recording-(recording_msid)-get-feedback>`_)
  scoreValidate(score)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/feedback/recording/$#/get-feedback" % recordingMsid, HttpGet, {"score": $score, "count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(Feedback)

proc getFeedbackForRecordings*(
  lb: ListenBrainz,
  username: string,
  recordings: seq[string]): Future[Feedback] {.fastsync.} =
  ## Get feedback for recording with given recording_msid.
  ##
  ## `GET /1/feedback/recording/(recording_msid)/get-feedback` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-feedback-user-(user_name)-get-feedback-for-recordings>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/feedback/user/$#/get-feedback-for-recordings" % username, HttpGet, {"recordings": join(recordings, ",")}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(Feedback)

proc getFeedback*(
  lb: ListenBrainz,
  username: string,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[Feedback] {.fastsync.} =
  ## Get feedback given by user username.
  ##
  ## `GET /1/feedback/user/(username)/get-feedback` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-feedback-user-(user_name)-get-feedback>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/feedback/user/$#/get-feedback" % username, HttpGet, {"count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(Feedback)

proc getFeedback*(
  lb: ListenBrainz,
  username: string,
  score: int,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[Feedback] {.fastsync.} =
  ## Get feedback given by user username.
  ##
  ## `GET /1/feedback/user/(username)/get-feedback` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-feedback-user-(user_name)-get-feedback>`_)
  scoreValidate(score)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/feedback/user/$#/get-feedback" % username, HttpGet, {"score": $score, "count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(Feedback)

proc pin*(
  lb: ListenBrainz,
  pinnedRecording: PinnedRecordingPayload): Future[PinnedRecording] {.fastsync.} =
  ## Pin a recording for user.
  ##
  ## `POST /1/pin` (`API documentation<https://listenbrainz.readthedocs.io/en/latest/dev/api/#post--1-pin>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/pin", HttpPost),
      body = pinnedRecording.toJson(),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(PinnedRecording)

proc deletePin*(
  lb: ListenBrainz,
  rowId: int): Future[string] {.fastsync.} =
  ## Deletes the pinned recording with given `rowId` from the server.
  ##
  ## `POST /1/pin/delete/(row_id)` (`API documentation<https://listenbrainz.readthedocs.io/en/latest/dev/api/#post--1-pin-delete-(row_id)>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/pin/delete/$#" % $rowId, HttpPost),
    )
    resp = await lb.request(req)
  return resp.body

proc getFollowerPins*(
  lb: ListenBrainz,
  username: string,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[PinnedRecordings] {.fastsync.} =
  ## Get a list containing the active pinned recordings for all users in a user’s following list.
  ##
  ## `GET /1/(username)/pins/following` (`API documentation<https://listenbrainz.readthedocs.io/en/latest/dev/api/#get--1-(user_name)-pins-following>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/$#/pins/following" % username, HttpGet, {"count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(PinnedRecordings)

proc getUserPins*(
  lb: ListenBrainz,
  username: string,
  count: int = DEFAULT_ITEMS_PER_GET,
  offset: int = 0): Future[PinnedRecordings] {.fastsync.} =
  ## Get a list of all recordings ever pinned by a user with given username in descending order of the time they were originally pinned.
  ##
  ## `GET /1/(username)/pins/` (`API documentation<https://listenbrainz.readthedocs.io/en/latest/dev/api/#get--1-(user_name)-pins>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/$#/pins" % username, HttpGet, {"count": $min(count, MAX_ITEMS_PER_GET), "offset": $offset}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(PinnedRecordings)

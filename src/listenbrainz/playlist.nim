import std/strutils
from std/httpcore import HttpMethod
import pkg/asyncutils
import utils/[api, client]
include utils/tools

proc createPlaylist*(
  lb: ListenBrainz,
  playlist: JSPFPayload): Future[string] {.fastsync.} =
  ## Create a playlist. The playlist must be in JSPF format with MusicBrainz extensions, which is defined here: https://musicbrainz.org/doc/jspf .
  ##
  ## `POST /1/playlist/create` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-playlist-create>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/playlist/create", HttpPost),
      body = playlist.toJson(),
    )
    resp = await lb.request(req)
  return resp.body

proc deletePlaylistItem*(
  lb: ListenBrainz,
  playlistMbid, recordingMbid: string,
  index, count: int): Future[string] {.fastsync.} =
  ## Delete an item in a playlist.
  ##
  ## `POST /1/playlist/(playlist_mbid)/item/delete` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-playlist-(playlist_mbid)-item-delete>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/playlist/$#/item/delete" % playlistMbid, HttpPost),
      body = """{"mbid": "$#", "index": $#, "count": $#}""" % [recordingMbid, $index, $min(count, MAX_ITEMS_PER_GET)],
    )
    resp = await lb.request(req)
  return resp.body

proc movePlaylistItem*(
  lb: ListenBrainz,
  playlistMbid, recordingMbid: string,
  moveFrom, to, count: int): Future[string] {.fastsync.} =
  ## Move an item in a playlist.
  ##
  ## `POST /1/playlist/(playlist_mbid)/item/move` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-playlist-(playlist_mbid)-item-move>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/playlist/$#/item/move" % playlistMbid, HttpPost),
      body = """{"mbid": "$#", "from": $#, "to": $#, "count": $#}""" % [recordingMbid, $moveFrom, $to, $min(count, MAX_ITEMS_PER_GET)],
    )
    resp = await lb.request(req)
  return resp.body

proc addPlaylistItem*(
  lb: ListenBrainz,
  playlistMbid: string,
  offset: int = 0,
  playlist: JSPFPayload): Future[string] {.fastsync.} =
  ## Append recordings to an existing playlist at an optional offset by posting a playlist with one of more recordings in it.
  ##
  ## `POST /1/playlist/(playlist_mbid)/item/add/(int: offset)` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-playlist-(playlist_mbid)-item-add-(int-offset)>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/playlist/$#/item/add/$#" % [playlistMbid, $offset], HttpPost),
      body = playlist.toJson(),
    )
    resp = await lb.request(req)
  return resp.body

proc editPlaylist*(
  lb: ListenBrainz,
  playlistMbid: string,
  playlist: JSPFPayload): Future[string] {.fastsync.} =
  ## Edit the utils/public status, name, description or list of collaborators for an exising playlist.
  ##
  ## `POST /1/playlist/edit/(playlist_mbid)` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-playlist-edit-(playlist_mbid)>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/playlist/edit/$#" % playlistMbid, HttpPost),
      body = playlist.toJson(),
    )
    resp = await lb.request(req)
  return resp.body

proc deletePlaylist*(
  lb: ListenBrainz,
  playlistMbid: string): Future[string] {.fastsync.} =
  ## Delete a playlist.
  ##
  ## `POST /1/playlist/(playlist_mbid)/delete` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-playlist-(playlist_mbid)-delete>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/playlist/$#/delete" % playlistMbid, HttpPost),
    )
    resp = await lb.request(req)
  return resp.body

proc copyPlaylist*(
  lb: ListenBrainz,
  playlistMbid: string): Future[string] {.fastsync.} =
  ## Copy a playlist – the new playlist will be given the name “Copy of <playlist_name>”.
  ##
  ## `POST /1/playlist/(playlist_mbid)/copy` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#post--1-playlist-(playlist_mbid)-copy>`_)
  lb.setContentType("application/json")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/playlist/$#/copy" % playlistMbid, HttpPost),
    )
    resp = await lb.request(req)
  return resp.body

proc getPlaylist*(
  lb: ListenBrainz,
  playlistMbid: string,
  fetchMetadata: bool = true): Future[JSPFPayload] {.fastsync.} =
  ## Fetch the given playlist.
  ##
  ## `GET /1/playlist/(playlist_mbid)` (`API documentation<https://listenbrainz.readthedocs.io/en/production/dev/api/#get--1-playlist-(playlist_mbid)>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/playlist/$#" % playlistMbid, HttpGet, {"fetch_metadata": $fetchMetadata}),
    )
    resp = await lb.request(req)
  return resp.body.fromJson(JSPFPayload)

from std/httpcore import HttpMethod
import pkg/asyncutils
import utils/client

proc getDumpInfo*(
  lb: ListenBrainz): Future[string] {.fastsync.} =
  ## Get information about latest ListenBrainz data dump.
  ##
  ## `GET /1/status/get-dump-info` (`API documentation<>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/status/get-dump-info", HttpGet),
    )
    resp = await lb.request(req)
  return resp.body

proc getDumpInfo*(
  lb: ListenBrainz,
  id: int): Future[string] {.fastsync.} =
  ## Get information about ListenBrainz data dumps.
  ##
  ## `GET /1/status/get-dump-info` (`API documentation<>`_)
  lb.setContentType("application/x-www-form-urlencoded")
  let
    req = newPureRequest(
      endpoint = build(lb.baseUrl, "/1/status/get-dump-info", HttpGet, {"id": $id}),
    )
    resp = await lb.request(req)
  return resp.body

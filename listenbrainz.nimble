# Package

version       = "0.2.1"
author        = "tandy1000 & Thomas Carroll"
description   = "Low-level multisync (C backend) and async (JS backend) bindings to the ListenBrainz web API."
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.5.0"
requires "jsony"
requires "nodejs"
requires "dotenv >= 2.0.0"
requires "https://github.com/alaviss/union >= 0.1.1"
requires "https://github.com/tandy-1000/uniony"
requires "https://github.com/tandy-1000/jspf-nim"
requires "https://github.com/tandy-1000/asyncutils"

task docs, "generate docs!":
  exec "rm -rf docs/"
  exec "nim doc --project --git.commit:master --git.devel:master --git.url:https://gitlab.com/tandy1000/listenbrainz-nim --outdir:docs src/listenbrainz.nim || true"
  exec "rm --verbose docs/listenbrainz/*.idx"
  exec "rm --verbose --force --recursive docs/nimcache/*.*"
  exec "mv --verbose docs/listenbrainz.html docs/index.html"

task buildjs, "build for JS target!":
  exec "nim js src/listenbrainz.nim"

**listenbrainz-nim**  [![pipeline status](https://gitlab.com/tandy1000/listenbrainz-nim/badges/master/pipeline.svg)](https://gitlab.com/tandy1000/listenbrainz-nim/-/commits/master) [![Build Status](https://nimble.directory/ci/badges/listenbrainz/nimdevel/status.svg)](https://nimble.directory/ci/badges/listenbrainz/nimdevel/output.html) [![Build Status](https://nimble.directory/ci/badges/listenbrainz/nimdevel/docstatus.svg)](https://nimble.directory/ci/badges/listenbrainz/nimdevel/doc_build_output.html)

```
import listenbrainz
import listenbrainz/[core, playlist, feedback, recommendation, stats, status, social]
```

This library contains sync / async bindings to the [ListenBrainz](https://listenbrainz.org) web API. The library returns API data as JSON strings.

**Features**:

- JavaScript backend: leverages `jsfetch` to function in the browser.
- [Jason Beetham](https://github.com/beef331/)'s `fastsync` pragma to provide sync / async bindings for C and JS
- [jsony](https://github.com/treeform/jsony/) for fast and easy conversion between API types and JSON
- [union](https://github.com/alaviss/union) (and [uniony](https://github.com/tandy-1000/uniony/)) to support type unions for accurate API types

The package is available on [Nimble](https://nimble.directory/pkg/listenbrainz).

Documentation is hosted [here](https://tandy1000.gitlab.io/listenbrainz-nim/).

**TODO:**

- [ ] Add [metadata](https://listenbrainz.readthedocs.io/en/latest/users/api/metadata.html) and [misc](https://listenbrainz.readthedocs.io/en/latest/users/api/misc.html) endpoints

Inspired by [lastfm-nim](https://gitlab.com/ryukoposting/lastfm-nim/).

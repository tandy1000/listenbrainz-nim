when defined(c):
  import
    std/[unittest, os],
    pkg/[listenbrainz, dotenv],
    pkg/listenbrainz/core

  load(".", os.getEnv("ENV"))

  let user = os.getEnv("LISTENBRAINZ_USER")
  var lb = newSyncListenbrainz()

  suite "Core":

    suite "Unauthenticated":
      setup:
        let user1 = os.getEnv("LISTENBRAINZ_USER_1")

      test "Invalid Token":
        let validateToken = lb.validateToken("invalid token")

      test "get Created For Playlists":
        let createdForPlaylists = lb.getCreatedForPlaylists(user)

      test "get Similar Users":
        let similarUser = lb.getSimilarUsers(user)

      test "get User Listen Count":
        let userListenCount = lb.getUserListenCount(user)

      test "get User Playing Now":
        let userPlayingNow = lb.getUserPlayingNow(user)

      test "get Users Similar To":
        let usersSimilarTo = lb.getUsersSimilarTo(user, user1)

      test "get User Listens":
        let userListens = lb.getUserListens(user)

      test "get Latest Import":
        let latestImport = lb.getLatestImport(user)

    suite "Authenticated":
      setup:
        let lbToken = os.getEnv("LISTENBRAINZ_TOKEN")
        lb = newSyncListenbrainz(lbToken)

      test "Valid Token":
        if lbToken != "":
          let validateToken = lb.validateToken(lbToken).valid

      test "get User Playlists":
        let userPlaylists = lb.getUserPlaylists(user)

      test "get Collaborative Playlists":
        let collaborativePlaylists = lb.getCollaborativePlaylists(user)

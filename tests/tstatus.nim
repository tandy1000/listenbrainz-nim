when defined(c):
  import
    std/unittest,
    pkg/listenbrainz,
    pkg/listenbrainz/status

  suite "Status":
    setup:
      let lb = newSyncListenbrainz()

    test "get Latest Dump Info":
      discard lb.getDumpInfo()

    test "get Dump Info for id = 1":
      discard lb.getDumpInfo(id = 1)

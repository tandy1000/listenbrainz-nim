when defined(c):
  import
    std/[unittest, json, options, os],
    pkg/[listenbrainz, dotenv],
    pkg/listenbrainz/social

  load(".", os.getEnv("ENV"))

  let user = os.getEnv("LISTENBRAINZ_USER")
  # var timelineEvent, timelineEvent1: UserTimelineEvent

  suite "Social":

    suite "Unauthenticated":
      setup:
        let lb = newSyncListenbrainz()

      test "get Followers":
        let followers = lb.getFollowers(user)

      test "get Following":
        let following = lb.getFollowing(user)

    suite "Authenticated":
      setup:
        let
          user1 = os.getEnv("LISTENBRAINZ_USER_1")
          lbToken = os.getEnv("LISTENBRAINZ_TOKEN")
          lb = newSyncListenbrainz(lbToken)
          successNode = %* {"status": "ok"}

      # test "recommend Personal":
      #   let recording = newAPITimelineRecommendation("Rick Astley", "Never Gonna Give You Up", "f06e84e0-0917-48a2-9549-22d4f8efaebc", users = @[lbUser1])
      #   timelineEvent = lb.recommendPersonal(user, recording)

      # test "post Message":
      #   check parseJson(lb.postMessage(user, "test")) == successNode

      # test "recommend Recording":
      #   let recording = newAPITimelineRecommendation("Rick Astley", "Never Gonna Give You Up", "f06e84e0-0917-48a2-9549-22d4f8efaebc")
      #   timelineEvent1 = lb.recommendRecording(user, recording)

      # test "post Review":
      #   check parseJson(lb.postReview(user, "test")) == successNode

      # test "delete Event":
      #   check parseJson(lb.deleteEvent(user, recordingRecommendation, get timelineEvent1.id)) == successNode

      # test "hide Event":
      #   check parseJson(lb.hideEvent(user, recordingRecommendation, get timelineEvent.id)) == successNode

      # test "unhide Event":
      #   check parseJson(lb.unhideEvent(user, recordingRecommendation, get timelineEvent.id)) == successNode

      test "get Events":
        let events = lb.getEvents(user)

      # add test for min ts
      # add test for max ts
      # add test for both

      test "get Followers":
        let followers = lb.getFollowers(user)

      test "get Following":
        let following = lb.getFollowing(user)

      test "Unfollow":
        check parseJson(lb.unfollow(user1)) == successNode

      test "Follow":
        check parseJson(lb.follow(user1)) == successNode

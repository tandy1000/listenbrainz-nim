when defined(c):
  import
    std/[unittest, json, os],
    pkg/[listenbrainz, dotenv],
    pkg/listenbrainz/recommendation

  load(".", os.getEnv("ENV"))

  let
    user = os.getEnv("LISTENBRAINZ_USER")
    recordingMbid = "a929c61a-2d27-41df-ad94-15839b7046f4"

  suite "Recommendation":

    suite "Authenticated":
      setup:
        let
          lbToken = os.getEnv("LISTENBRAINZ_TOKEN")
          lb = newSyncListenbrainz(lbToken)
          successNode = %* {"status": "ok"}

      test "submit Recommendation Feedback":
        check parseJson(lb.submitRecommendationFeedback(recordingMbid, rating = love)) == successNode

      test "delete Recommendation Feedback":
        check parseJson(lb.deleteRecommendationFeedback(recordingMbid)) == successNode

    suite "Unauthenticated":
      setup:
        let lb = newSyncListenbrainz()

      test "get User Recommendations":
        let userRecommendations = lb.getUserRecommendations(user, artistType = top)

      test "get Feedback For Recordings":
        let feedbackForRecordings = lb.getFeedbackForRecordings(user, recordings = @[recordingMbid])

      test "get Feedback":
        let feedback = lb.getFeedback(user)

      test "get Feedback with rating":
        let feedback = lb.getFeedback(user, rating = love)

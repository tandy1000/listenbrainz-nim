when defined(c):
  import
    std/[unittest, os],
    pkg/[listenbrainz, dotenv],
    pkg/listenbrainz/stats

  load(".", os.getEnv("ENV"))

  suite "Stats":
    setup:
      let
        user = os.getEnv("LISTENBRAINZ_USER")
        dateRange = StatsRangeType.week
        lb = newSyncListenbrainz()

    test "get Sitewide Top Artists":
      let sitewideTopArtists = lb.getSitewideTopArtists(dateRange)

    test "get User Listening Activity":
      let userListeningActivity = lb.getUserListeningActivity(user, dateRange)

    test "get User Daily Activity":
      let userDailyActivity = lb.getUserDailyActivity(user, dateRange)

    test "get User Top Recordings":
      let userTopRecordings = lb.getUserTopRecordings(user, dateRange)

    test "get User Artist Map":
      let userArtistMap = lb.getUserArtistMap(user, dateRange)

    test "get User Top Releases":
      let userTopReleases = lb.getUserTopReleases(user, dateRange)

    test "get User Top Artists":
      let userTopArtists = lb.getUserTopArtists(user, dateRange)

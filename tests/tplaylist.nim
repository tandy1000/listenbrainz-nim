when defined(c):
  import
    std/[unittest, json, options, os],
    pkg/[listenbrainz, dotenv],
    pkg/listenbrainz/playlist

  load(".", os.getEnv("ENV"))

  let recordingMbid = "8f3471b5-7e6a-48da-86a9-c1c07a0f47ae"
  var
    lb: SyncListenbrainz
    playlistMbid, playlistMbid1 = ""

  suite "Playlist":

    suite "Authenticated":
      setup:
        let
          user = os.getEnv("LISTENBRAINZ_USER")
          lbToken = os.getEnv("LISTENBRAINZ_TOKEN")
          successNode = %* {"status": "ok"}
        lb = newSyncListenbrainz(lbToken)

      test "create Playlist":
        let
          jspfPayload = newJSPFPayload(newPlaylist(title = "test", creator = some user))
          resp = parseJson lb.createPlaylist(jspfPayload)
        playlistMbid = getStr resp["playlist_mbid"]
        let status = getStr resp["status"]
        check status == "ok"

      test "add Playlist Item":
        let jspfPayload = newJSPFPayload(newPlaylist(title = "test", @[newTrack(identifier = recordingMbid)], identifier = some playlistMbid))
        check parseJson(lb.addPlaylistItem(playlistMbid, playlist = jspfPayload)) == successNode

      test "add Playlist Item with offset":
        let jspfPayload = newJSPFPayload(newPlaylist(title = "test", @[newTrack(identifier = recordingMbid)], identifier = some playlistMbid))
        check parseJson(lb.addPlaylistItem(playlistMbid, offset = 1, jspfPayload)) == successNode

      test "move Playlist Item":
        check parseJson(lb.movePlaylistItem(playlistMbid, recordingMbid, moveFrom = 0, to = 1, count = 1)) == successNode

      test "delete Playlist Item":
        check parseJson(lb.deletePlaylistItem(playlistMbid, recordingMbid, index = 1, count = 1)) == successNode

      test "copy Playlist":
        let resp = parseJson lb.copyPlaylist(playlistMbid)
        playlistMbid1 = getStr resp["playlist_mbid"]
        check getStr(resp["status"]) == "ok"

      test "edit Playlist":
        let jspfPayload = newJSPFPayload(newPlaylist(title = "test", creator = some user, identifier = some playlistMbid1))
        check parseJson(lb.editPlaylist(playlistMbid1, jspfPayload)) == successNode

      test "get Playlist":
        lb = newSyncListenbrainz()
        let playlist = lb.getPlaylist(playlistMbid)

      test "delete Playlist":
        check parseJson(lb.deletePlaylist(playlistMbid)) == successNode
        check parseJson(lb.deletePlaylist(playlistMbid1)) == successNode

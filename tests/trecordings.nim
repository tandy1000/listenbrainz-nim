when defined(c):
  import
    std/[unittest, json, times, options, os],
    pkg/[listenbrainz, dotenv],
    pkg/listenbrainz/recordings

  load(".", os.getEnv("ENV"))

  let recordingMsid = "a929c61a-2d27-41df-ad94-15839b7046f4"

  suite "Recordings":

    suite "Authenticated":
      setup:
        let
          lbToken = os.getEnv("LISTENBRAINZ_TOKEN")
          lb = newSyncListenbrainz(lbToken)
          successNode = %* {"status": "ok"}

      test "submit Recording Feedback":
        let feedback = newRecordingFeedback(score = 1, recordingMsid)
        check parseJson(lb.submitRecordingFeedback(feedback)) == successNode

      test "pin Recording and delete Pin":
        let
          time = int(toUnix getTime()) + 60
          pinnedRecording = lb.pin(newPinnedRecordingPayload(recordingMsid, "blurb", pinnedUntil = time)).pinnedRecording
        check parseJson(lb.deletePin(get pinnedRecording.rowId)) == successNode

    suite "Unauthenticated":
      setup:
        let
          lb = newSyncListenbrainz()
          user = os.getEnv("LISTENBRAINZ_USER")

      test "get Recording Feedback":
        let recordingFeedback = lb.getRecordingFeedback(recordingMsid)

      test "get Recording Feedback with score = 1":
        let recordingFeedback = lb.getRecordingFeedback(recordingMsid, score = 1)

      test "get Feedback For Recordings":
        let feedbackForRecordings = lb.getFeedbackForRecordings(user, @[recordingMsid])

      test "get Feedback":
        let feedback = lb.getFeedback(user)

      test "get Feedback with score = 1":
        let feedback = lb.getFeedback(user, score = 1)

      test "get Follower Pins":
        let followerPins = lb.getFollowerPins(user)

      test "get User Pins":
        let userPins = lb.getUserPins(user)
when defined(c):
  import std/unittest
  include pkg/listenbrainz/utils/tools

  suite "Tools":
    setup:
      type Test = object
        field: string
        optionInt*: Option[int]

    test "camel2snake":
      check Test(field: "", optionInt: some 1).toJson() == """{"field":"","option_int":1}"""

    test "Drop Options":
      check Test(field: "", optionInt: none int).toJson() == """{"field":""}"""

    test "Rename incoming playlist extension field":
      let
        a = """{"https://musicbrainz.org/doc/jspf#playlist":{"public":true}}""".fromJson(PlaylistExtension)
        b = newPlaylistExtension()
      check a.playlistExtensionUrl.creator == b.playlistExtensionUrl.creator and
        a.playlistExtensionUrl.createdFor == b.playlistExtensionUrl.createdFor and
        a.playlistExtensionUrl.collaborators == b.playlistExtensionUrl.collaborators and
        a.playlistExtensionUrl.copiedFrom == b.playlistExtensionUrl.copiedFrom and
        a.playlistExtensionUrl.lastModifiedAt == b.playlistExtensionUrl.lastModifiedAt and
        a.playlistExtensionUrl.copiedFromDeleted == b.playlistExtensionUrl.copiedFromDeleted and
        a.playlistExtensionUrl.public == b.playlistExtensionUrl.public

    test "Rename outgoing playlist extension field":
      check newPlaylistExtension().toJson() == """{"https://musicbrainz.org/doc/jspf#playlist":{"public":true}}"""

    test "Rename incoming track extension field":
      let
        a = """{"https://musicbrainz.org/doc/jspf#track":{}}""".fromJson(TrackExtension)
        b = newTrackExtension()
      check a.trackExtensionUrl.releaseIdentifier == b.trackExtensionUrl.releaseIdentifier and
        a.trackExtensionUrl.addedBy == b.trackExtensionUrl.addedBy and
        a.trackExtensionUrl.addedAt == b.trackExtensionUrl.addedAt and
        a.trackExtensionUrl.artistMbids == b.trackExtensionUrl.artistMbids

    test "Rename outgoing track extension field":
      check newTrackExtension().toJson() == """{"https://musicbrainz.org/doc/jspf#track":{}}"""